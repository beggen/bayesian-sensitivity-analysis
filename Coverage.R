#Seed for reproducability and loading necessary packages
#set.seed(22022024)
library(parallel)   #Parallel computation

#----------------Variables to change each run---------------------
n <- 100  #Observations
monte_carlo_samples <- 1000       #Amount of times the experiment is repated and quantiles are determined
num_cores <- 24                   #Number of cores to run parallel

#Prior on sensitivity parameter: normal distribution with mean and standard deviation
prioralphamean <- 2
prioralphasd <- 0.5

#Prior on eta: U[left,right]
prioretaleft <- -5
prioretaright <- 2

#Function that returns samples from the base measure of the DP in the H parametrisation.
#Note that we put non-zero mass on 0, as discussed in paper.
#Input:
#   n_sample:     Amount of samples to return
#Output:
#   Vector of length n_sample containing samples from the base measure
a_H <- function (n_sample) {
  a0 <- 0.4           #Probability of 0 in the base measure
  #Sample either from 0, or 1. Latter becomes location of sample from continuous part
  Z <- sample(x = 0:1,
              size = n_sample,
              replace = TRUE
              )
  Zequal1 <- Z == 1   #Logical vector of positions where Z == 1
  #Where Z == 1, sample from continuous part (YOUR GUESS FOR DISTRIBUTION OF H)
  #Make sure the amount of samples is sum(Zequal1)
  Z[Zequal1] <- rgamma(n = sum(Zequal1),
                       shape = a,
                       rate = b
                       )
  return(Z)
}

#Function that returns samples from the base measure of the DP in the P parametrisation.
#Input:
#   n_sample:     Amount of samples to return
#Output:
#   Vector of length n_sample containing samples from the base measure
a_P <- function (n_sample) {
  #Mixture of gammas as base measure (Oracle guess)
  
  #Sample the shape parameter
  shape_base <- sample(x = c(a+alpha0,a),
                       size = n_sample,
                       prob = c(1-p0,p0),
                       replace = TRUE
                       )
  #Sample from gamma where shape changes according to the vector shape_base
  Z <- rgamma(n = n_sample,
              shape = shape_base,
              rate = b
              )
  return(Z)
}

t <- n*10     #Truncation (*n needed to make the weights almost sum to 1)
M_H <- 1      #Prior precision for the base measure of the DP on H
a0 <- 0.4     #Initial probability on 0 for the base measure of the DP on H
M_P <- 1      #Prior precision for the base measure of the DP on P

#--------------------------Functions------------------------------

#Model for q: q(y) = alpha*(log(y) - E[log(Y)])
#Input:
#   y:      Positive real number or vector (including 0)
#   alpha:  Real number
#   c:      E[log(Y)], centering of the function. 0 in Scharfstein et al.
#Output:
#   Real number or vector 

qq <- function (y, alpha, meanlogy) {
  #When alpha = 0, function will return vector of zeros
  if (alpha == 0) {
    y <- rep(0,length(y))
  }
  #When alpha = 1. 
  else {
    
    y0 <- y == 0
    #Apply model for q
    y[!y0] <- alpha * (log(y[!y0]) - meanlogy)
    y[y0] <- -Inf
  }
  return(y)
}

#Draw one sample from the posterior functional of interest for a given alpha
#Input:
#   alpha:    Real number
#   Y:        Real Vector
#   a:        function that takes as input a natural number and outputs tis amount of samples from the base measure
#   M:        prior precision of a
#   t:        truncation of the DP stick-breaking
#   meanlogy: the centering value in the sensitivity function
#Output:
#   One draw from the posterior of the functional of interest in the H parametrisation
f_H <- function (alpha, Y, a, M, t, meanlogy) {
  #Sample from DP posterior
  DPsample <- DP(Y, a, M, t)
  
  #Calculate all necessary integrals
  #Note that integrals over multiplier empirical measures are just
  #sums of weights times the function in the locations.
  #DPsample[,1] is a vector of weights, DPsample[,2] vector of location
  Heq <- sum(DPsample[,1] * exp(qq(DPsample[,2],alpha, meanlogy)))
  Hyeq <- sum(DPsample[,1] * DPsample[,2] * exp(qq(DPsample[,2],alpha, meanlogy)))
  Hy <- sum(DPsample[,1] * DPsample[,2])
  
  DP0 <- DPsample[,2] == 0    #Vector of postitions where the location equals 0
  H0 <- sum(DPsample[DP0,1])  #Total mass of teh posterior in 0
  
  return(H0 * Hyeq / Heq + Hy)
}

#Simulate a draw from a posterior Dirichlet Process using Stick-Breacking
#Note that updated base measure is a + nP_n, with P_n empirical measure
#Input:
#   X:  Real vector
#   a:  function that takes as input a natural number and outputs tis amount of samples from the base measure
#   M:  prior precision of a
#   t:  truncation of the stick-breaking (sum_{i=1}^t W_i \delta_{Z_i})
#Output:
#   t x 2 matrix, first column containing the weights and second column the locations of the posterior
DP <- function (X, a, M, t) {
  n_data <- length(X)       #Size of observations (|nP_n|)
  
  #Sample from Beta distribution according to theory
  stick <- rbeta(n = t,
                 shape1 = 1,
                 shape2 = M + n_data
                 )
  
  #Calculate the cumulative product of the 1-stick weights
  product <- c(1, cumprod(1-stick[1:(t-1)]))
  
  #Final weights for the DP Process.
  W <- stick * product
  
  #Create vector of zeros and ones, zero will become sample from base measure, one from empirical measure.
  #Note that the probabilities in sample do not need to be normalised.
  Z <- sample(x = 0:1,
              size = t,
              replace = TRUE,
              prob = c(M,n_data)
              )
  Z0 <- Z == 0              #Logical vector of locations where Z = 0
  Z[Z0] <- a_H(sum(Z0))     #Sample from the base measure
  
  #Sample from empirical measure where Z = 1
  Z[!Z0] <- sample(x = X,
                   size = sum(!Z0),
                   replace = TRUE
                   )
  #Combine weights and locations in a t x 2 matrix
  WZ_combine <- matrix(data = c(W,Z),
                       nrow = t,
                       ncol = 2
                       )
  return(WZ_combine)
}

#Sample from the joint of P, alpha, eta, Yobs, Ymiss through the conditionals. One step in a Gibbs sampling scheme
#Input:
#   P:          d x 2 matrix of weights in the first column and real valued locations in the second (d can be any positive integer)
#   alpha:      real number
#   eta:        real number
#   Yobs:       real vector without missing observations
#   Ymiss       real vector of missing observations
#   a:          function with input a positive integer that samples the same amount of observations from the base measure of the prior DP
#   M:          real number, prior precision
#   t:          positive integer, truncation of the DP posterior
#   stepsize:   positive real number, standard deviation of the transition kernel in the RWMH
#   prioralpha: vector (R,R+) with the first coordinate the mean and second the standard deviation of the prior on alpha
#   prioreta:   vector (R,R) with the minimum and maximum of the uniform prior on eta (first coordinate has to be strictly smaller than the second)
#   meanlogy:   real number, the centering value in the sensitivity function
#Output:
#list containing (in that order)
#   P:          t x 2 matrix, first column the weights second the locations of the posterior DP
#   alpha:      new sample from the posterior of alpha
#   eta:        new sample from the posterior of eta
#   Ymiss:      vector containing the sampled missing values
#   outcome:    value of the functional of interest with these sepcific values of the parameters
SampleJointP <- function (P, alpha, eta, Yobs, Ymiss, a, M, t, stepsize, prioralpha, prioreta, meanlogy,n1){
  #Sample Ymiss | q, eta, F, O
  #This is sampling from P0 with replacement as a function of P, which is a DP
  Ymiss <- sample(x = P[,2],
                  size = n-n1,
                  replace = TRUE, 
                  prob = exp(eta+qq(P[,2],alpha,meanlogy))/(1 + exp(eta+qq(P[,2],alpha, meanlogy)))*P[,1]
                  )
  
  #Sample F | q, eta, Ymiss, O
  #This is a DP posterior, only dependent on the full data
  P <- DP(X = c(Yobs, Ymiss),
          a = a,
          M = M,
          t = t
          )
  
  #Sample q,eta | F, Ymiss, O
  #This has a likelihood if we condition on the complete data. We use RWMH
  #Sample a new alpha
  alphanew <- rnorm(n = 1,
                    mean = alpha,
                    sd = stepsize
                    )
  #Sample a new eta
  etanew <- rnorm(n = 1,
                  mean = eta,
                  sd = stepsize
                  )
  #Sample a standard uniform RV, to compare with the accept probability
  u <- runif(n = 1,
             min = 0,
             max = 1
             )
  
  #Calculate the ratio of the posteriors for the new and old values of alpha and eta
  #Logarithm to fix problems with high n
  logteller <- sum(c(-log(1 + exp(etanew + qq(Yobs,alphanew,meanlogy))),log(exp(etanew + qq(Ymiss,alphanew, meanlogy)))-log(1 + exp(etanew + qq(Ymiss,alphanew, meanlogy))))) + log(dunif(etanew, prioreta[1],prioreta[2])) + log(dnorm(alphanew, prioralpha[1],prioralpha[2]))
  lognoemer <- sum(c(-log(1 + exp(eta + qq(Yobs,alpha,meanlogy))),log(exp(eta + qq(Ymiss,alpha, meanlogy)))-log(1 + exp(eta + qq(Ymiss,alpha, meanlogy))))) + log(dunif(eta, prioreta[1],prioreta[2])) +log(dnorm(alpha,prioralpha[1],prioralpha[2]))
  
  #The accept probability is now the difference as we took the logarithm. If it is bigger than zero, we take 0
  acceptprob <- min(logteller - lognoemer,0)
  
  #Accept if the probability is bigger than u, else reject
  if (exp(acceptprob) > u) {
    eta <- etanew
    alpha <- alphanew
  }

  #Calculate the functional of interest, which is the mean of the posterior of P
  outcome <- sum(P[,1]*P[,2])
  
  #Combine results in a named list
  posterior <- list(P, alpha, eta, Ymiss, outcome)
  names(posterior) <- c("P", "alpha", "eta", "Ymiss", "outcome")
  
  return(posterior)
}
#Performs the same for each input
coverage <- function (i) {
  
  #----------------------------Sampling------------------------------
  R <- sample(0:1, prob = c(1-p0,p0), size = n, replace = TRUE)
  
  #Complete data
  Y <- rgamma(n = n,
              shape = shape[R+1],
              rate = b
  )
  
  X_obs <- R*Y                #Observed data
  Ymiss <- X_obs[X_obs!=0]    #Non-zero observations
  n1 <- length(Ymiss)         #Number of samples from P_1
  
  #----------------------------Parametrisation H--------------------------------
  #Sample from the posterior of alpha, which is the prior in this parametrisation
  alphapostH <- rnorm(n = draws,
                      mean = prioralpha[1],
                      sd = prioralpha[2]
  )
  #Parallel computed functional of interest for parametrisation H
  outcomeH <- sapply(X = alphapostH,
                     FUN = f_H,
                     Y = X_obs,
                     a = a_H,
                     M = M_H,
                     t = t,
                     meanlogy = meanlogy
                     )
  
  #-------------------------Parametrisation P ---------------------------
  
  #Implement a burn in of the chain
  draws <- draws + burnin
  
  #Initialise variables for the gibbs sampling
  etainit <- mean(prioreta)    #Initialise eta in the middle of the prior
  eta <- c(etainit, rep(0,draws-1))           #Create a vector where the draws will be stored
  alphainit <- prioralpha[1]                  #Initialise alpha at the prior mean
  alpha <- c(alphainit, rep(0,draws-1))       #Create a vector where the draws will be stored
  
  #Initialise the sample from P0 by first drawing them from the empirical of P1 according to the model
  Ymissinit <- sample(x = Ymiss,
                      size = n-n1,
                      replace = TRUE,
                      prob = exp(qq(Ymiss,alpha[1], meanlogy))
                      )
  Ppost <- c(Ymiss,Ymissinit)   #Initialise the posterior locations of P
  PpostW <- rep(1/n, n)         #Initialise the weights of the locations
  postP <- matrix(data = c(Ppost, PpostW),
                  nrow = n,
                  ncol = 2
                  )
  outcomeP <- rep(0,draws)      #Create vector where the samples from the functional of interest will be stored
  
  #Create a list containing all parameters. Prepared in such a way that it can be recursively used for the SampleJointP function
  posterior <- list(postP, alpha[1], eta[1], Ymissinit)
  #Give labels to the list elements for improved readability
  names(posterior) <- c("P", "alpha", "eta", "Ymiss")
  
  #---------------------Gibbs Sampling------------------------------
  for (i in 2:draws) {
    
    posterior <- SampleJointP(P = posterior$P,
                              alpha = posterior$alpha,
                              eta = posterior$eta,
                              Yobs = Ymiss,
                              Ymiss = posterior$Ymiss,
                              a = a_P,
                              M = M_P,
                              t = t,
                              stepsize = stepsize,
                              prioralpha = prioralpha,
                              prioreta = prioreta,
                              meanlogy = meanlogy,
                              n1 = n1)
    #Save values that are needed for analysis
    outcomeP[i] <- posterior$outcome
  }
  
  quantileH <- quantile(x = outcomeH,
                        probs = c(0.05,0.95)
                        )
  outcomeP2 <- outcomeP[(burnin+1):draws]
  quantileP <- quantile(x = outcomeP2,
                        probs = c(0.05,0.95)
                        )
  return(c(quantileH,quantileP))
}

#------------------------Initializers-----------------------------
#Vector containing the prior parameters for alpha
prioralpha <- c(prioralphamean, prioralphasd)
#Vector containing the prior parameters for eta
prioreta <- c(prioretaleft,prioretaright)

p0 <- 0.6 #True p
alpha0 <- 2 #True alpha

#Parameters for P1
a <- 2  #Shape
b <- 1  #Rate

shape <- c(a + alpha0,a)    #Shape in vector, (P0,P1)

#The true functional of interest (mean)
theoreticalmean <- sum(c(p0,1-p0)*c(a/b,(a+alpha0)/b))

#Mean of log(Y) in this model (Oracle)
#meanlogy <- p0*(psigamma(a) - log(b)) + (1-p0)*(psigamma(a+alpha0) - log(b))
meanlogy <- psigamma(a) - log(b)

#True eta in this model (Oracle)
eta0 <- log((1-p0))-log(p0) +alpha0*log(b) - log(gamma(a + alpha0)/gamma(a)) + alpha0*meanlogy

#Parameters for good MCMC mixing 
if (n <= 100) {
  stepsize <- 0.7     #Standard deviation of RWMH transition kernel
  draws <- 5000       #Gibbs samples
  burnin <- 1000       #How many samples to throw away in the MCMC scheme for P
} else {
  if (n <= 1000) {
    stepsize <- 0.21
    draws <- 10000
    burnin <- 5000
  } else {
    stepsize <- 0.08
    draws <- 100000
    burnin <- 5000
  }
}


#Calculate the quantiles of the posterior outcomes of the H and P parametrisations
quantiles <- mclapply(X = 1:monte_carlo_samples,
                      FUN = coverage,
                      mc.cores = num_cores
                      )
#Convert to a matrix format for easier access
#4 x monte carlo samples matrix, first two rows contain the quantiles for H, last two rows for P
quantiles <- matrix(unlist(quantiles), nrow = 4, ncol = monte_carlo_samples)

coverageH <- sum((quantiles[1,] <= theoreticalmean)*(quantiles[2,] >= theoreticalmean))/10000
coverageP <- sum((quantiles[3,] <= theoreticalmean)*(quantiles[4,] >= theoreticalmean))/10000
lengthH <- sum(quantiles[2,] - quantiles[1,])/10000
lengthP <- sum(quantiles[4,] - quantiles[3,])/10000

df <- data.frame(coverageH = coverageH,
                 lengthH = lengthH,
                 coverageP = coverageP,
                 lengthP = lengthP
                 )
write.table(x = df,
            file = "parallelresults.csv",
            append = TRUE,
            sep = ",",
            col.names = FALSE,
            row.names = FALSE
            )