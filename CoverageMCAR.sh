#!/bin/sh

#SBATCH --job-name="CoverageMCAR"
#SBATCH --partition=compute
#SBATCH --account=innovation
#SBATCH --time=00:15:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=12
#SBATCH --mem-per-cpu=1GB

module load 2022r2
module load r
srun Rscript Coverage.R > Coverage.log